package com.gi.xiot;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.gi.xiot.data.ConnectionContract;
import com.gi.xiot.data.ConnectionDbHelperMethods;
import com.gi.xiot.data.SubscribeContract;
import com.gi.xiot.data.SubscribeDbHelperMethods;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.HashMap;
import java.util.Map;

/*
 * This is the connection factory which is responsible to build all connections on start using database.
 *
 * The factory is built using singleton and factory design patterns.
 *
 * The factory is also responsible to store all the clients in a map.
 *
 * The map is available to all other activities and they can edit and delete from it.
 */
public class ConnectionFactory {

    // private due to singleton design pattern.
    private static ConnectionFactory connectionFactory;

    private Map<String, MqttAndroidClient> clinetsMap = new HashMap<String, MqttAndroidClient>();

    private MqttAndroidClient client;

    // Help in reading connection database.
    private ConnectionDbHelperMethods connectionHelperMethods;

    // Store the cursor from ConnectionDbHelperMethods containing all connection database information.
    private Cursor connectionCursor;

    // Figure out the index of each column while reading cursor from connection database.
    private int clientIdColumnIndex;
    private int serverColumnIndex;
    private int portColumnIndex;
    private int cleanSessionColumnIndex;
    private int userNameColumnIndex;
    private int passwordColumnIndex;
    private int sslColumnIndex;
    private int timeOutColumnIndex;
    private int keepAliveTimeOutColumnIndex;
    private int statusColumnIndex;

    // Store data currently read from connection database cursor.
    private String currentClientId;
    private String currentServer;
    private String currentPort;
    private int currentCleanSession;
    private String currentUserName;
    private String currentPassword;
    private String currentFilePath;
    private int currentTimeOut;
    private int currentKeepAliveTimeOut;
    private String currentStatus;

    // Build url for connecting the client.
    private String url;

    // Build options for connecting the client.
    private MqttConnectOptions options;

    // Help in reading subscriptions database.
    private SubscribeDbHelperMethods subscripeHelperMethods;

    // Store the cursor from SubscribeDbHelperMethods containing all connection database information.
    private Cursor subscriptionCursor;

    // Figure out the index of each column while reading cursor from subscription database.
    private int subscriptionClientIdColumnIndex;
    private int subscriptionTopicColumnIndex;
    private int subscriptionQosColumnIndex;

    // Store data currently read from subscription database cursor.
    private String subscriptionCurrentClientId;
    private String subscriptionCurrentTopic;
    private int subscriptionCurrentQos;

    private IMqttToken token;

    // private due to singleton design pattern.
    private ConnectionFactory() {
    }

    // Built due to singleton design pattern.
    public static ConnectionFactory getConnectionFactory(){
        if(connectionFactory == null){
            connectionFactory = new ConnectionFactory();
        }

        Log.d("connection factory" ,"connection factory is called");

        return connectionFactory;
    }

    // Build all the clients connection on app start using database status.
    public void BuildClientsMap(Context context){

        clinetsMap.clear();

        Log.d("connection factory" ,"start building clients map");

        // Initialize connection database helper.
        connectionHelperMethods = new ConnectionDbHelperMethods(context);

        Log.d("connection factory" ,"connection helper method is initialized");

        // Gets connections database in read mode.
        connectionCursor = connectionHelperMethods.readDataBase();

        Log.d("connection factory" ,"connection cursor is now ready with data");

        try {
            // Figure out the index of each column.
            clientIdColumnIndex = connectionCursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_CLIENT_ID);
            serverColumnIndex = connectionCursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_SERVER);
            portColumnIndex = connectionCursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_PORT);
            cleanSessionColumnIndex = connectionCursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_CLEAN_SESSION);
            userNameColumnIndex = connectionCursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_USER_NAME);
            passwordColumnIndex = connectionCursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_PASSWORD);
            sslColumnIndex = connectionCursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_SSL);
            timeOutColumnIndex = connectionCursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_TIME_OUT);
            keepAliveTimeOutColumnIndex = connectionCursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_KEEP_ALIVE_TIME);
            statusColumnIndex = connectionCursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_STATUS);

            Log.d("connection factory" ,"connection index of each column was figured");

            while (connectionCursor.moveToNext()) {
                // Use that index to extract the String or Int value of the word
                // at the current row the connectionCursor is on.
                currentStatus = connectionCursor.getString(statusColumnIndex);

                Log.d("connection factory" ,"read client from database");

                // Checking if it need to be connected or not.
                if(currentStatus.equals("C")){

                    Log.d("connection factory" ,"client need to be connected");

                    currentClientId = connectionCursor.getString(clientIdColumnIndex);
                    currentServer = connectionCursor.getString(serverColumnIndex);
                    currentPort = connectionCursor.getString(portColumnIndex);;
                    currentCleanSession = connectionCursor.getInt(cleanSessionColumnIndex);
                    currentUserName = connectionCursor.getString(userNameColumnIndex);
                    currentPassword = connectionCursor.getString(passwordColumnIndex);
                    currentFilePath = connectionCursor.getString(sslColumnIndex);
                    currentTimeOut = connectionCursor.getInt(timeOutColumnIndex);
                    currentKeepAliveTimeOut = connectionCursor.getInt(keepAliveTimeOutColumnIndex);

                    Log.d("connection factory" ,"client data extracted");

                    buildUrl();

                    Log.d("connection factory" ,"url built");

                    EstablishConnection(context);

                    Log.d("connection factory" ,"client created");

                    setOptions();

                    Log.d("connection factory" ,"options done");

                    //try {
                        //client.connect(options);
                    connectClient(context);

                    Log.d("connection factory" ,"client added to map");
                }
            }
        } finally {
            connectionCursor.close();
        }
    }

    // Build url which is used to connect the client.
    private void buildUrl(){
        url = "tcp://" + currentServer + ":" + currentPort;
    }

    // Create client object.
    private void EstablishConnection(Context context){
        client = new MqttAndroidClient(context, url, currentClientId);
    }

    // Setting options to be used to connect client.
    private void setOptions(){
        options = new MqttConnectOptions();
        options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);

        if(!currentUserName.equals("") && !currentPassword.equals("")){
            options.setUserName(currentUserName);
            options.setPassword(currentPassword.toCharArray());
        }
    }

    private void connectClient(final Context context){
        try {
            token = client.connect(options);

            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.d("connection factory" ,"client connected");
                    clinetsMap.put(currentClientId, client);
                    addSubscriptions(context);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    // Assummed to not happen here.
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    // Add all clients subscriptions on app start.
    // It can be implemented in a betetr way but it was a big problem which
    // I finally solved.
    public void addSubscriptions(Context context){

        Log.d("connection factory" ,"start adding subscriptions to clients");

        // Initialize connection database helper.
        subscripeHelperMethods = new SubscribeDbHelperMethods(context);

        Log.d("connection factory" ,"subscription helper method is initialized");

        // Gets subscriptions database in read mode.
        subscriptionCursor = subscripeHelperMethods.readDataBase();

        Log.d("connection factory" ,"subscription cursor is now ready with data");

        try {
            // Figure out the index of each column.
            subscriptionClientIdColumnIndex = subscriptionCursor.getColumnIndex(SubscribeContract.SubscribeEntry.COLUMN_CLIENT_ID);
            subscriptionTopicColumnIndex = subscriptionCursor.getColumnIndex(SubscribeContract.SubscribeEntry.COLUMN_TOPIC);
            subscriptionQosColumnIndex = subscriptionCursor.getColumnIndex(SubscribeContract.SubscribeEntry.COLUMN_QOS);

            Log.d("connection factory" ,"subscription index of each column was figured");

            while (subscriptionCursor.moveToNext()) {
                // Use that index to extract the String or Int value of the word
                // at the current row the connectionCursor is on.
                subscriptionCurrentClientId = subscriptionCursor.getString(subscriptionClientIdColumnIndex);
                subscriptionCurrentTopic = subscriptionCursor.getString(subscriptionTopicColumnIndex);
                subscriptionCurrentQos = subscriptionCursor.getInt(subscriptionQosColumnIndex);

                Log.d("connection factory" ,"subscription data extracted");

                try {
                    if(clinetsMap.containsKey(subscriptionCurrentClientId) &&
                            !clinetsMap.get(subscriptionCurrentClientId).equals(null)){
                        getSpecificClient(subscriptionCurrentClientId, context).subscribe(subscriptionCurrentTopic
                                , subscriptionCurrentQos);
                        Log.d("connection factory" ,"subscription is added");
                    }

                    Log.d("connection factory" ,"subscription done");
                } catch (MqttException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            subscriptionCursor.close();
        }
    }

    // Return specific client from map if connected or null if not.
    // If condition can be omitted.
    public MqttAndroidClient getSpecificClient(String clientId , Context context) {
        if(clinetsMap.containsKey(clientId)){
            return clinetsMap.get(clientId);
        }
        return null;
    }

    public void addClientToMap(String clientId , MqttAndroidClient client){
        if(!clinetsMap.containsKey(clientId)){
            clinetsMap.put(clientId , client);
        }
    }

    // If condition can be omitted.
    public void removeSpecificClientFromMap(String clientId){
        if(clinetsMap.containsKey(clientId)){
            clinetsMap.remove(clientId);
        }
    }

    public void removeAllClientsFromMap(){
        clinetsMap.clear();
    }
}

