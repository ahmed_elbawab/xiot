package com.gi.xiot;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/*
 * This is the main activity of the app.
 *
 * Main activity is responsible to call the connection factory
 * to build all the connections which were connecting before closing the app.
 *
 * Comments are only used on need due to clean code reference.
 *
 * All log message are for debugging and can be commented or deleted out.
 */
public class MainActivity extends AppCompatActivity {

    // TODO : Last will messages
    // TODO : Unsubscribe activity

    // TODO : error 1 notification system
    // TODO : error 2 publish history
    // TODO : error 3 subscribe history
    // TODO : error 4 connected history
    // TODO : error 5 close and open the app lose history
    // TODO : error 6 close and open the app lose connections
    // TODO : error 7 no unsbscribe
    // TODO : error 8 disconnect client with same id

    private Button buildConnectionButton;
    private Button aboutXiotButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("main activity" , "main activity has started");

        // Build all connections again and add all subscriptions.
        final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
        connectionFactory.BuildClientsMap(getApplicationContext());
        //connectionFactory.addSubscriptions(getApplicationContext());

        Log.d("main activity" , "building connections has finished");

        // Initialize buttons from screen.
        buildConnectionButton = (Button) findViewById(R.id.build_connecton_button);
        aboutXiotButton = (Button) findViewById(R.id.about_xiot_button);

        Log.d("main activity" , "buttons binded");

        buildConnectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("main activity" , "build connection button clicked");

                startActivity(new Intent(MainActivity.this , ConnectionListActivity.class));
            }
        });

        aboutXiotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("main activity" , "about xiot button clicked");

                // Instantiate an AlertDialog.Builder with its constructor
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(MainActivity.this);
                }

                // Chain together various setter methods to set the dialog characteristics
                builder.setMessage(R.string.main_activity_dialog_message)
                        .setTitle(R.string.main_activity_dialog_title);

                // Add the buttons
                builder.setPositiveButton(R.string.main_activity_dialog_visit_website_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String url = String.valueOf(R.string.main_activity_dialog_website_button_url);
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(url));
                        PackageManager packageManager = MainActivity.this.getPackageManager();
                        if(intent.resolveActivity(packageManager) != null){
                            startActivity(intent);
                        }else{
                            Toast toast = Toast.makeText(MainActivity.this,
                                    R.string.main_activity_dialog_toast_fail_message, Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                });
                builder.setNegativeButton(R.string.main_activity_dialog_cancel_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                // Get the AlertDialog from create()
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }
}
