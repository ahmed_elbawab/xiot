package com.gi.xiot.Mqtt;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.gi.xiot.data.SubscribeContract;
import com.gi.xiot.data.SubscribeDbHelper;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttException;

/*
 * This do the subscribe operation using mqtt subscribe.
 */
public class MqttSubscribe {

    // Store data to subscribe topic.
    private MqttAndroidClient client;
    private String topic;
    private int qos;

    // To add subscription to database.
    private Context context;

    // Used to avoid multiple operation callback.
    private boolean flag;

    public MqttSubscribe(MqttAndroidClient client , String topic , int qos , Context context){
        Log.d("mqtt subscribe" , "mqtt subscribe is called");

        this.client = client;
        this.topic = topic;
        this.qos = qos;

        this.context = context;

        this.flag = false;

        Log.d("mqtt subscribe" , "data is set");

        Subscribe();
    }

    private void Subscribe(){
        try {
            Log.d("mqtt subscribe" , "subscribe is called");

            IMqttToken subToken = client.subscribe(topic, qos);

            Log.d("mqtt subscribe" , "trying to subscribe");

            subToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // The topic was subscribed
                    if(!flag){
                        Log.d("mqtt subscribe" , "topic is subscribed");

                        flag = true;

                        insertSubscribtion();

                        Toast.makeText(context, "Subscribtion is done", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    // The subscription could not be performed, maybe the user was not
                    // authorized to subscribe on the specified topic e.g. using wildcards
                    if(!flag){
                        Log.d("mqtt subscribe" , "can't subscrib");

                        flag = true;

                        Toast.makeText(context, "Sorry can't subscribe!", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void insertSubscribtion(){
        Log.d("mqtt subscribe" , "insert subscription is called");

        // Create database helper
        SubscribeDbHelper mDbHelper = new SubscribeDbHelper(context);

        Log.d("mqtt subscribe" , "helper method is done");

        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("mqtt subscribe" , "cursor in write mode");

        // Create a ContentValues object where column names are the keys,
        // and pet attributes from the editor are the values.
        ContentValues values = new ContentValues();
        values.put(SubscribeContract.SubscribeEntry.COLUMN_CLIENT_ID, client.getClientId());
        values.put(SubscribeContract.SubscribeEntry.COLUMN_TOPIC, topic);
        values.put(SubscribeContract.SubscribeEntry.COLUMN_QOS, qos);

        Log.d("mqtt subscribe" , "values are done");

        // Insert a new row for pet in the database, returning the ID of that new row.
        long newRowId = db.insert(SubscribeContract.SubscribeEntry.TABLE_NAME, null, values);

        Log.d("mqtt subscribe" , "insertion is done");

        // Show a toast message depending on whether or not the insertion was successful
        if (newRowId == -1) {
            // If the row ID is -1, then there was an error with insertion.
            Toast.makeText(context, "Error while doing subscribtion try again!", Toast.LENGTH_LONG).show();
        } else {
            // Otherwise, the insertion was successful and we can display a toast with the row ID.
            Toast.makeText(context, "Subscribtion is done", Toast.LENGTH_LONG).show();
        }

    }
}
