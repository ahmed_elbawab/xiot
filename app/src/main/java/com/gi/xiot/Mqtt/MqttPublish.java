package com.gi.xiot.Mqtt;

import android.util.Log;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;

/*
 * This do the publish operation using mqtt publish.
 */
public class MqttPublish {

    // Store data to publish messages.
    private MqttAndroidClient client;
    private String topic;
    private String payload;
    private int qos;
    private int retained;

    public MqttPublish(MqttAndroidClient client , String topic , String message , int qos , int retained){
        Log.d("mqtt publish" , "mqtt publish is called");

        this.client = client;
        this.topic = topic;
        this.payload = message;
        this.qos = qos;
        this.retained = retained;

        Log.d("mqtt publish" , "data is set");

        Publish();
    }

    private void Publish(){

        Log.d("mqtt publish" , "publish is called");

        // Due to mqtt website.
        byte[] encodedPayload = new byte[0];

        try {
            // Due to mqtt website.
            encodedPayload = payload.getBytes("UTF-8");

            MqttMessage message = new MqttMessage(encodedPayload);

            Log.d("mqtt publish" , "message is ready");

            if(retained == 1){
                message.setRetained(true);
            }else{
                message.setRetained(false);
            }

            Log.d("mqtt publish" , "retained is done");

            message.setQos(qos);

            Log.d("mqtt publish" , "qos is done");

            client.publish(topic, message);

            Log.d("mqtt publish" , "message is published");

        } catch (UnsupportedEncodingException | MqttException e) {
            e.printStackTrace();
        }
    }
}
