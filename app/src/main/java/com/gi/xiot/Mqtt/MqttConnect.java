package com.gi.xiot.Mqtt;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import com.gi.xiot.ConnectionFactory;
import com.gi.xiot.data.ConnectionDbHelperMethods;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

/*
 * This is responsible to do real client connection using mqtt connect.
 *
 * It is also responsible to change connection database status on connect.
 *
 * It is also responsible to add the client to the factory map.
 */
public class MqttConnect {

    private MqttAndroidClient client;

    private IMqttToken  token;

    private MqttConnectOptions options;

    // Store client connection data.
    private String clientId;
    private String server;
    private String port;
    private String userName;
    private String password;

    private Context context;

    // Store url to be used in connection.
    private String url;

    // Help in reading and updating database.
    private ConnectionDbHelperMethods helperMethods;

    // Store the cursor from helperMethods containing all connection database information.
    private Cursor cursor;

    // Figure out the index of each column while reading cursor from connection database.
    private int clientIdColumnIndex;
    private int idColumnIndex;

    // Store data currently read from connection database cursor.
    private String currentClientId;
    private String currentId;

    // Used to avoid multiple operation callback.
    private boolean flag;

    // TODO : handel -1 time out and keep alive

    public MqttConnect(String clientId , String server , String port, String userName , String password , Context context ){
        this.clientId = clientId;
        this.server = server;
        this.port = port;
        this.userName = userName;
        this.password = password;
        this.context = context;

        this.flag = false;

        Log.d("mqtt connect" , "mqtt connect was called");

        buildUrl();

        Log.d("mqtt connect" , "url are done");

        EstablishConnection(context);

        Log.d("mqtt connect" , "client are done");

        setOptions();

        Log.d("mqtt connect" , "options are done");

        buildConnection();
    }

    private void buildUrl(){
        url = "tcp://" + server + ":" + port;
    }

    private void EstablishConnection(Context context){
        client = new MqttAndroidClient(context, url, clientId);
    }

    private void setOptions(){
        options = new MqttConnectOptions();
        options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);

        if(!userName.equals("") && !password.equals("")){
            options.setUserName(userName);
            options.setPassword(password.toCharArray());
        }
    }

    private void buildConnection(){
        try {

            Log.d("mqtt connect" , "build connection was called");

            Toast.makeText(context, "Please wait while connecting!", Toast.LENGTH_SHORT).show();

            Log.d("mqtt connect" , "client is trying to connect");

            token = client.connect(options);

            Log.d("mqtt connect" , "token are initialized");

            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    if(!flag){

                        flag = true;

                        Log.d("mqtt connect" , "client is connected");

                        setdatabaseStatus();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    if(!flag){

                        flag = true;

                        Log.d("mqtt connect" , "client can't connect");

                        setdatabaseStatus();
                    }
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void setdatabaseStatus(){
        Log.d("mqtt connect" , "set status are called");

        if(client.isConnected()){
            Log.d("mqtt connect" , "client is connected");

            updateDataBase();

            Log.d("mqtt connect" , "database is updated");

            upDateClientsMap();

            Log.d("mqtt connect" , "client map is updated");

            Toast.makeText(context, "You are connected", Toast.LENGTH_LONG).show();
        }else{
            Log.d("mqtt connect" , "client isn't connected");

            Toast.makeText(context, "Failed to connect try again!", Toast.LENGTH_LONG).show();
        }
    }

    public  void updateDataBase(){
        helperMethods = new ConnectionDbHelperMethods(context);
        helperMethods.upDateDatabaseStatus(clientId , "C");
    }

    private void upDateClientsMap(){
        Log.d("mqtt connect" , "update map is called");

        final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
        connectionFactory.addClientToMap(clientId , client);

        Log.d("mqtt connect" , "update map is done");
    }
}
