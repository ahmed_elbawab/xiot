package com.gi.xiot.Mqtt;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.gi.xiot.data.ConnectionContract;
import com.gi.xiot.data.ConnectionDbHelperMethods;

/*
 * This is mqtt connect adapter build on adapter design pattern principal.
 *
 * On messages list we don't have any information else client id
 * and mqtt connect need more information so here comes the adapter
 * to get this information and pass it to mqtt connect.
 */
public class MqttConnectAdapter {

    // Distictive client id passed from messages list.
    private String clientId;

    // Figure out the index of each column while reading cursor from connections database.
    private int clientIdColumnIndex;
    private int serverColumnIndex;
    private int portColumnIndex;
    private int userNameColumnIndex;
    private int passwordColumnIndex;

    // Store data currently read from connections database cursor.
    private String currentClientId;
    private String currentServer;
    private String currentPort;
    private String currentUserName;
    private String currentPassword;

    // Help in reading database.
    private ConnectionDbHelperMethods helperMethods;

    // Store the cursor from helperMethods containing all messages database information.
    private Cursor cursor;

    // Mqtt object to pass data to it to do connect.
    private MqttConnect connector;

    public MqttConnectAdapter(String clientId , Context context){
        Log.d("mqtt connect adapter" , "mqtt connect adapter was called");

        // Getting client id.
        this.clientId = clientId;

        Log.d("mqtt connect adapter" , "client id was set");

        // Initialize connection helper method to read database.
        helperMethods = new ConnectionDbHelperMethods(context);

        Log.d("mqtt connect adapter" , "helper method was done");

        // Getting connection data.
        setConnectionRequiredData();

        Log.d("mqtt connect adapter" , "connection data is done");

        // Do connection.
        connector = new MqttConnect(currentClientId , currentServer , currentPort ,
               currentUserName , currentPassword , context);
    }

    private void setConnectionRequiredData() {
        cursor = helperMethods.readDataBase();

        Log.d("mqtt connect adapter" , "cursor is ready with data");

        try {
            // Figure out the index of each column
            clientIdColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_CLIENT_ID);
            serverColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_SERVER);
            portColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_PORT);
            userNameColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_USER_NAME);
            passwordColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_PASSWORD);

            Log.d("mqtt connect adapter" , "indexes were figured out");

            while (cursor.moveToNext()) {
                // Use that index to extract the String or Int value of the word
                // at the current row the cursor is on.
                currentClientId = cursor.getString(clientIdColumnIndex);

                if(clientId.equals(currentClientId)){
                    Log.d("mqtt connect adapter" , "client was found");

                    currentServer = cursor.getString(serverColumnIndex);
                    currentPort = cursor.getString(portColumnIndex);
                    currentUserName = cursor.getString(userNameColumnIndex);
                    currentPassword = cursor.getString(passwordColumnIndex);

                    Log.d("mqtt connect adapter" , "client information was fetched");

                    break;
                }
            }
        } finally {
            cursor.close();
        }
    }
}
