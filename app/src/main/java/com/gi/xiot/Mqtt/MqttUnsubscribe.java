package com.gi.xiot.Mqtt;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.gi.xiot.data.SubscribeDbHelperMethods;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttException;

/*
 * This do the unsubscribe operation using mqtt unsubscribe.
 */
public class MqttUnsubscribe {

    // Store data to subscribe topic.
    private MqttAndroidClient client;
    private String topic;

    private Context context;

    // Used to avoid multiple operation callback.
    private boolean flag;

    // To delete subscribe from subscription database.
    private SubscribeDbHelperMethods subscribeDbHelperMethods;

    public MqttUnsubscribe(MqttAndroidClient client , String topic , Context context){
        Log.d("mqtt unsubscribe" , "mqtt unsubscribe is called");

        this.client = client;
        this.topic = topic;

        this.context = context;

        this.flag = false;

        Log.d("mqtt unsubscribe" , "data is set");

        unSubscribe();
    }

    private void unSubscribe(){
        try {
            Log.d("mqtt unsubscribe" , "unsubscribe is called");

            IMqttToken unsubToken = client.unsubscribe(topic);

            Log.d("mqtt unsubscribe" , "trying to unsubscribe");

            unsubToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // The subscription could successfully be removed from the client
                    if(!flag){
                        Log.d("mqtt unsubscribe" , "topic is unsubscribed");

                        flag = true;

                        deleteSubscription();

                        Toast.makeText(context, "Subscribtion is done", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    // some error occurred, this is very unlikely as even if the client
                    // did not had a subscription to the topic the unsubscribe action
                    // will be successfully
                    if(!flag){
                        Log.d("mqtt unsubscribe" , "can't unsubscrib");

                        flag = true;

                        Toast.makeText(context, "Sorry can't unsubscribe!", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void deleteSubscription(){
        subscribeDbHelperMethods.deleteSpecificSubscription(topic);
    }
}
