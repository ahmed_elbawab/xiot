package com.gi.xiot.Mqtt;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.gi.xiot.ConnectionFactory;
import com.gi.xiot.data.ConnectionDbHelperMethods;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttException;

/*
 * This do the disconnect operation using mqtt disconnect.
 */
public class MqttDisconnect {

    // Store data to subscribe topic.
    private MqttAndroidClient client;
    private String clientId;

    private Context context;

    // Used to avoid multiple operation callback.
    private boolean flag;

    // To change status in connection database.
    ConnectionDbHelperMethods connectionDbHelperMethods;

    public MqttDisconnect(MqttAndroidClient client , String clientId , Context context){
        Log.d("mqtt disconnect" , "mqtt disconnect is called");

        this.client = client;
        this.clientId = clientId;

        this.context = context;

        this.flag = false;

        connectionDbHelperMethods = new ConnectionDbHelperMethods(context);

        Log.d("mqtt disconnect" , "data is set");

        disConnect();
    }

    private void disConnect(){
        try {
            Log.d("mqtt disconnect" , "disconenct is called");

            IMqttToken disconToken = client.disconnect();

            Log.d("mqtt disconnect" , "trying to disconnect");

            disconToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // we are now successfully disconnected
                    if(!flag){
                        Log.d("mqtt disconnect" , "client is disconnected");

                        flag = true;

                        deleteSubscriptionFromMap();

                        changeConnectionTableStatus();

                        Toast.makeText(context, "Client is disconnected", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    // something went wrong, but probably we are disconnected anyway
                    if(!flag){
                        Log.d("mqtt disconnect" , "can't disconnect");

                        flag = true;

                        Toast.makeText(context, "Sorry can't disconnect!", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void deleteSubscriptionFromMap(){
        final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
        connectionFactory.removeSpecificClientFromMap(clientId);
    }

    private void changeConnectionTableStatus(){
        connectionDbHelperMethods.upDateDatabaseStatus(clientId , "D");
    }
}
