package com.gi.xiot;

/*
 * This an object which store connection data and passed to the adapter list.
 */
public class ConnectionItem {

    private String status;

    private String clientId;
    private String server;
    private String port;
    private int cleanSession;

    private String userName;
    private String password;
    private String filePath;
    private int timeOut;
    private int keepAliveTimeOut;

    public ConnectionItem(String clientId , String server , String port , int cleanSession ,
                          String userName , String password , String filePath , int timeOut ,
                          int keepAliveTimeOut , String status){
        this.clientId = clientId;
        this.server = server;
        this.port = port;
        this.userName = userName;
        this.password = password;
        this.filePath = filePath;
        this.timeOut = timeOut;
        this.keepAliveTimeOut = keepAliveTimeOut;
        this.status = status;
    }

    public String getClientId(){
        return this.clientId;
    }

    public String getServer() {
        return server;
    }

    public int getCleanSession() {
        return cleanSession;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getFilePath() {
        return filePath;
    }

    public int getTimeOut() {
        return timeOut;
    }

    public int getKeepAliveTimeOut() {
        return keepAliveTimeOut;
    }

    public String getStatus() {
        return status;
    }

}
