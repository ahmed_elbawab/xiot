package com.gi.xiot;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gi.xiot.Mqtt.MqttPublish;
import com.gi.xiot.data.MessageDbHelperMethods;

import org.eclipse.paho.android.service.MqttAndroidClient;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/*
 * Responsible to get publish messages data from user.
 *
 * Responsible to save message to messages database.
 *
 * "P" is used to indicate published messages.
 */
public class PublishActivity extends AppCompatActivity {

    // Distinctive client id passed from messages list.
    private String clientId;

    // Spinner used to get qos value from user.
    private Spinner mSpiner;

    // Store spinner value.
    private int qosValuse;

    // For binding data from screen.
    private EditText topicEditView;
    private EditText messageEditView;
    private CheckBox retainedCheckBox;

    // Publish messages.
    private MqttPublish publisher;

    // Used to publish messages.
    private MqttAndroidClient client;

    // Store data from user.
    private String topic;
    private String message;
    private int retained;

    // Used to insert in messages database.
    // Get current date and time to insert data on message arrive.
    private Calendar calendar;
    private SimpleDateFormat formater;
    private String date;
    private String time;

    // Help in inserting message to database.
    MessageDbHelperMethods messageDbHelperMethods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish);

        Log.d("publish activity" , "publish activity has started");

        // Get client id passed from messages list.
        clientId = getIntent().getStringExtra("clienId");

        Log.d("publish activity" , "client id recieved");

        // Get client from factory map to use it later in publishing.
        final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
        this.client = connectionFactory.getSpecificClient(clientId , getApplicationContext());

        Log.d("publish activity" , "client is fetched from map");

        topicEditView = (EditText) findViewById(R.id.publish_topic_edit_view);
        messageEditView = (EditText) findViewById(R.id.publish_message_edit_view);
        retainedCheckBox = (CheckBox) findViewById(R.id.publish_retaind_check_box);

        mSpiner = (Spinner) findViewById(R.id.publish_spinner_qos);

        setupSpinner();
    }

    /**
     * Setup the dropdown spinner that allows the user to select the qos.
     */
    private void setupSpinner() {
        // Create adapter for spinner. The list options are from the String array it will use
        // the spinner will use the default layout.
        ArrayAdapter qosSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_publish_qos_options, android.R.layout.simple_spinner_item);

        // Specify dropdown layout style - simple list view with 1 item per line.
        qosSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // Apply the adapter to the spinner.
        mSpiner.setAdapter(qosSpinnerAdapter);

        // Set the integer mSelected to the constant values.
        mSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.publish_activity_qos_spinner_option_0))) {
                        qosValuse = 0;
                    } else if (selection.equals(getString(R.string.publish_activity_qos_spinner_option_1))) {
                        qosValuse = 1;
                    } else {
                        qosValuse = 2;
                    }
                }
            }

            // Because AdapterView is an abstract class, onNothingSelected must be defined.
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                qosValuse = 0;
            }
        });
    }

    // Binding option menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.activity_publish_menu, menu);
        return true;
    }

    // Setting item on click for option menu.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.publish_activity_menu_done:
                Log.d("publish activity" , "done button is clicked");

                bindData();

                Log.d("publish activity" , "data is binded");

                if(checkValidity()){
                    Log.d("publish activity" , "data is valid");

                    publisher = new MqttPublish(client , topic , message , qosValuse , retained);

                    Log.d("publish activity" , "message is published");

                    // Get current date.
                    calendar = Calendar.getInstance();
                    formater = new SimpleDateFormat("yyyy / MM / dd ");
                    date = formater.format(calendar.getTime());
                    // Get current time.
                    formater = new SimpleDateFormat("HH:mm:ss");
                    time = formater.format(calendar.getTime());

                    messageDbHelperMethods = new MessageDbHelperMethods(getApplicationContext());
                    long newRowId = messageDbHelperMethods.insertMessage(clientId , topic , message , date , time , "P");

                    // Show a toast message depending on whether or not the insertion was successful
                    if (newRowId == -1) {
                        // If the row ID is -1, then there was an error with insertion.
                        Toast.makeText(this, "Message was't published try again!", Toast.LENGTH_LONG).show();
                    } else {
                        // Otherwise, the insertion was successful and we can display a toast with the row ID.
                        Toast.makeText(this, "Message was sent", Toast.LENGTH_LONG).show();
                    }

                    finish();
                }else {
                    Toast.makeText(this, "Wrong data can't publish!", Toast.LENGTH_LONG).show();
                }
                return true;
            // Respond to a click on the "Cancel" menu option
            case R.id.publish_activity_mnue_cancel:
                // Exit activity
                finish();
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void bindData(){
        topic = topicEditView.getText().toString().trim();
        message = messageEditView.getText().toString().trim();
        if(retainedCheckBox.isChecked()){
            retained = 1;
        }else{
            retained = 0;
        }
    }

    private boolean checkValidity(){
        if(!topic.matches("") && !message.matches("")){
            return true;
        }
        return false;
    }
}
