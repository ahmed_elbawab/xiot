package com.gi.xiot;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gi.xiot.data.ConnectionContract;
import com.gi.xiot.data.ConnectionDbHelper;

/*
 * This is responsible to get data from advanced edit connection activity.
 *
 * On done click data is inserted into connection database.
 *
 * Connection is inserted with default "D" disconnected.
 */
public class ConnectionAdvancedEditActivity extends AppCompatActivity {

    // For binding data from the screen.
    private Button chooseFileButton;
    private EditText userNameEditText;
    private EditText passwordEditText;
    private EditText timeOutEditText;
    private EditText keepAliveTimeOutEditText;

    // Values from edit connection activity.
    private String clientId;
    private String server;
    private String port;
    private int cleanSession;

    // Values edited in the screen.
    private String userName;
    private String password;
    private String filePath;
    private int timeOut;
    private int keepAliveTimeOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_advanced_edit);

        Log.d("connectionAdEd activity" , "connection edit activity has started");

        chooseFileButton = (Button) findViewById(R.id.choose_file_button);
        userNameEditText = (EditText) findViewById(R.id.user_name_edit_view);
        passwordEditText = (EditText) findViewById(R.id.password_edit_view);
        timeOutEditText = (EditText) findViewById(R.id.time_out_edit_view);
        keepAliveTimeOutEditText = (EditText) findViewById(R.id.keep_alive_time_out_edit_view);

        // Can't be done now as the available libraries are
        // too old so need to be implemented manually.
        chooseFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO : open file chooser
            }
        });

    }

    // Binding option menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.activity_connection_advanced_edit_menu, menu);
        return true;
    }

    // Setting item on click for option menu.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu.
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option.
            case R.id.connection_advanced_edit_activity_mnue_save_connection_button:
                Log.d("connectionAdEd activity" , "save connection button clicked");

                bindDataFromScreen();

                Log.d("connectionAdEd activity" , "data binded from screen");

                insertConnectionToDatabase();

                Log.d("connectionAdEd activity" , "data is inserted to database");

                // Return to connection list actiivity.
                startActivity(new Intent(ConnectionAdvancedEditActivity.this , ConnectionListActivity.class));
                return true;
            // Respond to a click on the "Delete" menu option.
            case R.id.connection_advanced_edit_activity_mnue_cancel_button:
                // Exit activity.
                finish();
                return true;
            // Respond to a click on the "Up" arrow button in the app bar.
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity).
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void bindDataFromScreen(){
        clientId = getIntent().getStringExtra("clientId");
        server = getIntent().getStringExtra("server");
        port = getIntent().getStringExtra("port");
        if(getIntent().getStringExtra("cleanSession") == null){
            cleanSession = 0;
        }else{
            cleanSession = Integer.parseInt(getIntent().getStringExtra("cleanSession"));
        }

        // TODO : bind data from file path
        filePath = "test";

        userName = userNameEditText.getText().toString().trim();
        password = passwordEditText.getText().toString().trim();
        if(timeOutEditText.getText().toString().trim().matches("")){
            timeOut = -1;
        }else{
            timeOut = Integer.parseInt(timeOutEditText.getText().toString().trim());
        }
        if(keepAliveTimeOutEditText.getText().toString().trim().matches("")){
            keepAliveTimeOut = -1;
        }else{
            keepAliveTimeOut = Integer.parseInt(keepAliveTimeOutEditText.getText().toString().trim());
        }
    }

    private void insertConnectionToDatabase(){
        Log.d("connectionAdEd activity" ,"start inserting connection");

        // Create database helper
        ConnectionDbHelper mDbHelper = new ConnectionDbHelper(getApplicationContext());

        Log.d("connectionAdEd activity" ,"connection helper is initialized");

        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("connectionAdEd activity" ,"database in write mode");

        // Create a ContentValues object where column names are the keys,
        // and pet attributes from the editor are the values.
        ContentValues values = new ContentValues();
        values.put(ConnectionContract.ConnectionEntry.COLUMN_CLIENT_ID, clientId);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_SERVER, server);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_PORT, port);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_CLEAN_SESSION, cleanSession);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_USER_NAME, userName);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_PASSWORD, password);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_SSL, filePath);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_TIME_OUT, timeOut);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_KEEP_ALIVE_TIME, keepAliveTimeOut);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_STATUS, "D");

        Log.d("connectionAdEd activity" ,"values are ready with data");

        // Insert a new row for pet in the database, returning the ID of that new row.
        long newRowId = db.insert(ConnectionContract.ConnectionEntry.TABLE_NAME, null, values);

        Log.d("connectionAdEd activity" ,"insertion done");

        // Show a toast message depending on whether or not the insertion was successful
        if (newRowId == -1) {
            // If the row ID is -1, then there was an error with insertion.
            Toast.makeText(this, "Error with establishing connection", Toast.LENGTH_LONG).show();
        } else {
            // Otherwise, the insertion was successful and we can display a toast with the row ID.
            Toast.makeText(this, "Connection saved with id: " + newRowId, Toast.LENGTH_LONG).show();
        }
    }
}
