package com.gi.xiot;

import android.content.Context;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

/*
 * Responsible to make system notification.
 */
public class Notification {

    private String title;
    private String text;

    private Context context;

    public Notification(String title ,String text , Context context){
        this.title = title;
        this.text = text;

        this.context = context;

        buildNotification();
    }

    private void buildNotification(){
        //Get an instance of NotificationManager//

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.color_white)
                        .setContentTitle(title)
                        .setContentText(text);

        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(1, mBuilder.build());
    }
}
