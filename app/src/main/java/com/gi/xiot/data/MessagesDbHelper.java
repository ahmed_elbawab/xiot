package com.gi.xiot.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Message database helper which is responsible to create and return database.
 */
public class MessagesDbHelper extends SQLiteOpenHelper {

    /** Name of the database file */
    private static final String DATABASE_NAME = "messages.db";

    /**
     * Database version. If you change the database schema, you must increment the database version.
     */
    private static final int DATABASE_VERSION = 1;

    public MessagesDbHelper(Context context){
        super(context , DATABASE_NAME , null , DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_MESSAGE_TABLE =  "CREATE TABLE " + MessagesContract.MessageEntry.TABLE_NAME + " ("
                + MessagesContract.MessageEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + MessagesContract.MessageEntry.COLUMN_CLIENT_ID + " TEXT NOT NULL, "
                + MessagesContract.MessageEntry.COLUMN_MESSAGE + " TEXT NOT NULL, "
                + MessagesContract.MessageEntry.COLUMN_TOPIC + " TEXT NOT NULL, "
                + MessagesContract.MessageEntry.COLUMN_DATE + " TEXT NOT NULL, "
                + MessagesContract.MessageEntry.COLUMN_TIME + " TEXT NOT NULL, "
                + MessagesContract.MessageEntry.COLUMN_TYPE + " TEXT NOT NULL);";

        // Execute the SQL statement
        db.execSQL(SQL_CREATE_MESSAGE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Do nothing for now
    }
}
