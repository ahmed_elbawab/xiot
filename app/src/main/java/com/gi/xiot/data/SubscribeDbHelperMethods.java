package com.gi.xiot.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Subscription Helper Method is responsible to read the subscription database
 * and return a cursor containing data.
 *
 * It is also responsible to delete all subscriptions entries.
 *
 * It is also responsible to delete a specific client subscriptions from database.
 *
 * Is is also responsible to delete a specific subscription from database.
 */
public class SubscribeDbHelperMethods {

    private SubscribeDbHelper mDbHelper;

    private Cursor cursor;

    private Context context;

    public SubscribeDbHelperMethods(Context context){

        Log.d("subscribe help method" ,"subscribe helper method is called");

        this.context = context;
    }

    public Cursor readDataBase(){
        mDbHelper = new SubscribeDbHelper(context);

        Log.d("subscribe help method" ,"read database is called");

        // Create and/or open a database to read from it
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Log.d("subscribe help method" ,"database in read mode");

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        // can be replaced with null
        String[] projection = {
                SubscribeContract.SubscribeEntry._ID,
                SubscribeContract.SubscribeEntry.COLUMN_CLIENT_ID,
                SubscribeContract.SubscribeEntry.COLUMN_TOPIC,
                SubscribeContract.SubscribeEntry.COLUMN_QOS};

        Log.d("subscribe help method" ,"projections in defined");

        // Perform a query on the connection table
        cursor = db.query(
                SubscribeContract.SubscribeEntry.TABLE_NAME,   // The table to query
                projection,            // The columns to return
                null,                  // The columns for the WHERE clause
                null,                  // The values for the WHERE clause
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);                 // The sort order

        Log.d("subscribe help method" ,"query is done");

        return cursor;
    }

    public void dropDatabase(){
        mDbHelper = new SubscribeDbHelper(context);

        Log.d("subscribe help method" ,"drop database is called");

        // Create and/or open a database to read from it.
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("subscibe help method" ,"database in read mode");

        //db.delete(SubscribeContract.SubscribeEntry.TABLE_NAME, null , null);
        db.execSQL("delete from "+ SubscribeContract.SubscribeEntry.TABLE_NAME);

        Log.d("subscribe help method" ,"database deleted");
    }

    public void deleteSpecificClientSubscription(String clientId){

        mDbHelper = new SubscribeDbHelper(context);

        Log.d("subscribe help method" ,"delete client subscribtion is called");

        // Create and/or open a database to read from it.
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("subscribe help method" ,"database in write mode");

        String[] whereArgs = new String[] { clientId };

        Log.d("subscribe help method" ,"where args are done");

        db.delete(SubscribeContract.SubscribeEntry.TABLE_NAME, SubscribeContract.SubscribeEntry.COLUMN_CLIENT_ID + "=?" ,
                whereArgs);

        Log.d("subscribe help method" ,"delete is over");
    }

    public void deleteSpecificSubscription(String topic){

        mDbHelper = new SubscribeDbHelper(context);

        Log.d("subscribe help method" ,"delete subscribtion is called");

        // Create and/or open a database to read from it.
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("subscribe help method" ,"database in write mode");

        String[] whereArgs = new String[] { topic };

        Log.d("subscribe help method" ,"where args are done");

        db.delete(SubscribeContract.SubscribeEntry.TABLE_NAME, SubscribeContract.SubscribeEntry.COLUMN_TOPIC + "=?" ,
                whereArgs);

        Log.d("subscribe help method" ,"delete is over");
    }
}
