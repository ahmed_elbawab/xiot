package com.gi.xiot.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Message Helper Method is responsible to read the messages database
 * and return a cursor containing data.
 *
 * It is also responsible to delete all messages entries.
 *
 * It is also responsible to delete a specific client messages from database.
 *
 * Is is also responsible to delete a specific message from database.
 *
 * It is also responsible to add messages to messages database.
 */
public class MessageDbHelperMethods {

    private MessagesDbHelper mDbHelper;

    private Cursor cursor;

    private Context context;

    public MessageDbHelperMethods(Context context){
        Log.d("messages help method" ,"messages helper method is called");

        this.context = context;
    }

    public Cursor readDataBase(){

        mDbHelper = new MessagesDbHelper(context);

        // Create and/or open a database to read from it
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Log.d("messages help method" ,"read database is called");

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        // can be replaced with null
        String[] projection = {
                MessagesContract.MessageEntry._ID,
                MessagesContract.MessageEntry.COLUMN_CLIENT_ID,
                MessagesContract.MessageEntry.COLUMN_MESSAGE,
                MessagesContract.MessageEntry.COLUMN_TOPIC,
                MessagesContract.MessageEntry.COLUMN_DATE,
                MessagesContract.MessageEntry.COLUMN_TIME,
                MessagesContract.MessageEntry.COLUMN_TYPE};

        Log.d("messages help method" ,"projections in defined");

        // Perform a query on the connection table
        cursor = db.query(
                MessagesContract.MessageEntry.TABLE_NAME,   // The table to query
                projection,            // The columns to return
                null,                  // The columns for the WHERE clause
                null,                  // The values for the WHERE clause
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);                 // The sort order

        Log.d("messages help method" ,"query is done");

        return cursor;
    }

    public void dropDatabase(){
        mDbHelper = new MessagesDbHelper(context);

        Log.d("message help method" ,"drop database is called");

        // Create and/or open a database to read from it.
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("message help method" ,"database in write mode");

        //db.delete(MessagesContract.MessageEntry.TABLE_NAME, null , null);
        db.execSQL("delete from "+ MessagesContract.MessageEntry.TABLE_NAME);

        Log.d("message help method" ,"database deleted");
    }

    public void deleteSpecificClientMessages(String clientId){

        mDbHelper = new MessagesDbHelper(context);

        Log.d("message help method" ,"delete client message is called");

        // Create and/or open a database to read from it.
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("message help method" ,"database in write mode");

        String[] whereArgs = new String[] { clientId };

        Log.d("message help method" ,"where args are done");

        db.delete(MessagesContract.MessageEntry.TABLE_NAME, MessagesContract.MessageEntry.COLUMN_CLIENT_ID + "=?" ,
                whereArgs);

        Log.d("message help method" ,"delete is over");
    }

    public void deleteSpecificMessage(String message){
        mDbHelper = new MessagesDbHelper(context);

        Log.d("message help method" ,"delete message is called");

        // Create and/or open a database to read from it.
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("message help method" ,"database in write mode");

        String[] whereArgs = new String[] { message };

        Log.d("message help method" ,"where args are done");

        db.delete(MessagesContract.MessageEntry.TABLE_NAME, MessagesContract.MessageEntry.COLUMN_MESSAGE + "=?" ,
                whereArgs);

        Log.d("message help method" ,"delete is over");
    }

    public long insertMessage(String clientId , String topic , String message , String date , String time , String type){
        // Create database helper
        MessagesDbHelper mDbHelper = new MessagesDbHelper(context);

        Log.d("message help method" ,"insert message is called");

        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("message help method" ,"database in write mode");

        // Create a ContentValues object where column names are the keys,
        // and pet attributes from the editor are the values.
        ContentValues values = new ContentValues();
        values.put(MessagesContract.MessageEntry.COLUMN_CLIENT_ID, clientId);
        values.put(MessagesContract.MessageEntry.COLUMN_MESSAGE, message);
        values.put(MessagesContract.MessageEntry.COLUMN_TOPIC, topic);
        values.put(MessagesContract.MessageEntry.COLUMN_DATE, date);
        values.put(MessagesContract.MessageEntry.COLUMN_TIME, time);
        values.put(MessagesContract.MessageEntry.COLUMN_TYPE, type);

        Log.d("message help method" ,"values are done");

        // Insert a new row for pet in the database, returning the ID of that new row.
        long newRowId = db.insert(MessagesContract.MessageEntry.TABLE_NAME, null, values);

        return  newRowId;
    }
}
