package com.gi.xiot.data;

import android.provider.BaseColumns;

/**
 * Subscription database schema.
 */
public class SubscribeContract {

    private SubscribeContract(){}

    public static abstract class SubscribeEntry implements BaseColumns {
        public static final String TABLE_NAME = "subscriptions";

        public static final String COLUMN_ID = BaseColumns._ID;
        public static final String COLUMN_CLIENT_ID = "client";
        public static final String COLUMN_TOPIC = "topic";
        public static final String COLUMN_QOS = "qos";

    }

}
