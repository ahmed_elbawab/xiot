package com.gi.xiot.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Connection database helper which is responsible to create and return database.
 */
public class ConnectionDbHelper extends SQLiteOpenHelper {

    /** Name of the database file */
    private static final String DATABASE_NAME = "connections.db";

    /**
     * Database version. If you change the database schema, you must increment the database version.
     */
    private static final int DATABASE_VERSION = 1;

    public ConnectionDbHelper(Context context){
        super(context , DATABASE_NAME , null , DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_CONNECTION_TABLE =  "CREATE TABLE " + ConnectionContract.ConnectionEntry.TABLE_NAME + " ("
                + ConnectionContract.ConnectionEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ConnectionContract.ConnectionEntry.COLUMN_CLIENT_ID + " TEXT NOT NULL, "
                + ConnectionContract.ConnectionEntry.COLUMN_SERVER + " TEXT NOT NULL, "
                + ConnectionContract.ConnectionEntry.COLUMN_PORT + " TEXT NOT NULL, "
                + ConnectionContract.ConnectionEntry.COLUMN_CLEAN_SESSION + " INTEGER DEFAULT 0, "
                + ConnectionContract.ConnectionEntry.COLUMN_USER_NAME + " TEXT, "
                + ConnectionContract.ConnectionEntry.COLUMN_PASSWORD + " TEXT, "
                + ConnectionContract.ConnectionEntry.COLUMN_SSL + " TEXT, "
                + ConnectionContract.ConnectionEntry.COLUMN_TIME_OUT + " INTEGER, "
                + ConnectionContract.ConnectionEntry.COLUMN_KEEP_ALIVE_TIME + " INTEGER, "
                + ConnectionContract.ConnectionEntry.COLUMN_STATUS + " TEXT NOT NULL);";

        // Execute the SQL statement
        db.execSQL(SQL_CREATE_CONNECTION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Do nothing for now
    }
}
