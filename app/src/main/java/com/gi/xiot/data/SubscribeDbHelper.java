package com.gi.xiot.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Subscription database helper which is responsible to create and return database.
 */
public class SubscribeDbHelper extends SQLiteOpenHelper {

    /** Name of the database file */
    private static final String DATABASE_NAME = "subscriptions.db";

    /**
     * Database version. If you change the database schema, you must increment the database version.
     */
    private static final int DATABASE_VERSION = 1;

    public SubscribeDbHelper(Context context){
        super(context , DATABASE_NAME , null , DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_SUBSCRIPTIONS_TABLE =  "CREATE TABLE " + SubscribeContract.SubscribeEntry.TABLE_NAME + " ("
                + SubscribeContract.SubscribeEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + SubscribeContract.SubscribeEntry.COLUMN_CLIENT_ID + " TEXT NOT NULL, "
                + SubscribeContract.SubscribeEntry.COLUMN_TOPIC + " TEXT NOT NULL, "
                + SubscribeContract.SubscribeEntry.COLUMN_QOS + " INTEGER DEFAULT 0);";

        // Execute the SQL statement
        db.execSQL(SQL_CREATE_SUBSCRIPTIONS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Do nothing for now
    }
}
