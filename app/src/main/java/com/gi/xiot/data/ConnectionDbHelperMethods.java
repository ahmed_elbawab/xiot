package com.gi.xiot.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Connection Helper Method is responsible to read the connection database
 * and return a cursor containing data.
 *
 * It is also responsible to delete all database entries.
 *
 * It is also responsible to delete a specific connection from database.
 *
 * It is also responsible to update database status.
 */
public class ConnectionDbHelperMethods {

    private ConnectionDbHelper mDbHelper;

    private Cursor cursor;

    private Context context;

    public ConnectionDbHelperMethods(Context context){

        Log.d("connection help method" ,"connection helper method is called");


        this.context = context;
    }

    public Cursor readDataBase(){

        mDbHelper = new ConnectionDbHelper(context);

        Log.d("connection help method" ,"read database is called");

        // Create and/or open a database to read from it.
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Log.d("connection help method" ,"database in read mode");

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        // can be replaced with null for now.
        String[] projection = {
                ConnectionContract.ConnectionEntry._ID,
                ConnectionContract.ConnectionEntry.COLUMN_CLIENT_ID,
                ConnectionContract.ConnectionEntry.COLUMN_SERVER,
                ConnectionContract.ConnectionEntry.COLUMN_PORT,
                ConnectionContract.ConnectionEntry.COLUMN_CLEAN_SESSION,
                ConnectionContract.ConnectionEntry.COLUMN_USER_NAME,
                ConnectionContract.ConnectionEntry.COLUMN_PASSWORD,
                ConnectionContract.ConnectionEntry.COLUMN_SSL,
                ConnectionContract.ConnectionEntry.COLUMN_TIME_OUT,
                ConnectionContract.ConnectionEntry.COLUMN_KEEP_ALIVE_TIME,
                ConnectionContract.ConnectionEntry.COLUMN_STATUS};

        Log.d("connection help method" ,"projections in defined");

        // Perform a query on the connection table
        cursor = db.query(
                ConnectionContract.ConnectionEntry.TABLE_NAME,   // The table to query
                projection,            // The columns to return
                null,                  // The columns for the WHERE clause
                null,                  // The values for the WHERE clause
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);                 // The sort order

        Log.d("connection help method" ,"query is done");

        return cursor;
    }

    public void dropDatabase(){

        mDbHelper = new ConnectionDbHelper(context);

        Log.d("connection help method" ,"drop database is called");

        // Create and/or open a database to read from it.
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("connection help method" ,"database in write mode");

        //db.delete(ConnectionContract.ConnectionEntry.TABLE_NAME, null , null);
        db.execSQL("delete from "+ ConnectionContract.ConnectionEntry.TABLE_NAME);

        Log.d("connection help method" ,"database deleted");
    }

    public void deleteSpecificConnection(String clientId){

        mDbHelper = new ConnectionDbHelper(context);

        Log.d("connection help method" ,"delete connection is called");

        // Create and/or open a database to read from it.
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("connection help method" ,"database in write mode");

        String[] whereArgs = new String[] { clientId };

        Log.d("connection help method" ,"where args are done");

        db.delete(ConnectionContract.ConnectionEntry.TABLE_NAME, ConnectionContract.ConnectionEntry.COLUMN_CLIENT_ID + "=?" ,
                whereArgs);

        Log.d("connection help method" ,"delete is over");
    }

    public void upDateDatabaseStatus(String clientId , String status){
        Log.d("connection help method" , "update database is called");

        // Create database helper
        ConnectionDbHelper mDbHelper = new ConnectionDbHelper(context);

        Log.d("connection help method" , "helper method is done");

        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("connection help method" , "cursor in write mode");

        // Create a ContentValues object where column names are the keys,
        // and pet attributes from the editor are the values.
        ContentValues values = new ContentValues();
        values.put(ConnectionContract.ConnectionEntry.COLUMN_STATUS, status);

        Log.d("connection help method" , "values are done");

        String[] whereArgs = new String[] { clientId };

        Log.d("connection help method" , "where args are done");

        db.update(ConnectionContract.ConnectionEntry.TABLE_NAME ,values
                , ConnectionContract.ConnectionEntry.COLUMN_CLIENT_ID + "=?", whereArgs);

        Log.d("connection help method" , "update database is done");
    }
}
