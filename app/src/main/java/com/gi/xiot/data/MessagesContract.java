package com.gi.xiot.data;

import android.provider.BaseColumns;

/**
 * Messages database schema.
 */
public class MessagesContract {

    private MessagesContract(){}

    public static abstract class MessageEntry implements BaseColumns {
        public static final String TABLE_NAME = "messages";

        public static final String COLUMN_ID = BaseColumns._ID;
        public static final String COLUMN_CLIENT_ID = "client";
        public static final String COLUMN_MESSAGE = "message";
        public static final String COLUMN_TOPIC = "topic";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_TIME = "time";
        public static final String COLUMN_TYPE = "type";
    }

}
