package com.gi.xiot.data;

import android.provider.BaseColumns;

/**
 * Connection database schema.
 */
public class ConnectionContract {

    private ConnectionContract(){}

    public static abstract class ConnectionEntry implements BaseColumns{
        public static final String TABLE_NAME = "connections";

        public static final String COLUMN_ID = BaseColumns._ID;
        public static final String COLUMN_CLIENT_ID = "client";
        public static final String COLUMN_SERVER = "server";
        public static final String COLUMN_PORT = "port";
        public static final String COLUMN_CLEAN_SESSION = "session";
        public static final String COLUMN_USER_NAME = "user";
        public static final String COLUMN_PASSWORD= "password";
        public static final String COLUMN_SSL = "ssl";
        public static final String COLUMN_TIME_OUT = "out";
        public static final String COLUMN_KEEP_ALIVE_TIME = "alive";

        public static final String COLUMN_STATUS = "status";

    }
}
