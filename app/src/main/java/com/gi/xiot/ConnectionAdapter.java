package com.gi.xiot;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/*
 * Connection adapter to pass connection data to screen.
 */
public class ConnectionAdapter extends ArrayAdapter<ConnectionItem> {

    public ConnectionAdapter(Activity context, ArrayList<ConnectionItem> androidFlavors) {
        super(context, 0, androidFlavors);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.connection_item, parent, false);
        }

        ConnectionItem currentConnection = getItem(position);

        TextView clientTextView = (TextView) listItemView.findViewById(R.id.client_text_view);
        clientTextView.setText(currentConnection.getClientId());

        TextView serverTextView = (TextView) listItemView.findViewById(R.id.server_text_view);
        serverTextView.setText(currentConnection.getServer());

        TextView statusTextView = (TextView) listItemView.findViewById(R.id.status_text_view);
        statusTextView.setText(currentConnection.getStatus());

        ImageView statusImageView = (ImageView) listItemView.findViewById(R.id.status_circle);
        if(currentConnection.getStatus().equals("D")){
            statusImageView.setImageResource(R.drawable.color_red);
        }else{
            statusImageView.setImageResource(R.drawable.color_green);
        }

        return listItemView;
    }

}
