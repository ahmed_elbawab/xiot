package com.gi.xiot;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.gi.xiot.Mqtt.MqttConnectAdapter;
import com.gi.xiot.Mqtt.MqttDisconnect;
import com.gi.xiot.data.ConnectionDbHelperMethods;
import com.gi.xiot.data.MessageDbHelperMethods;
import com.gi.xiot.data.MessagesContract;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/*
 * This is a client messages list activity.
 *
 * Published messages are "P".
 *
 * Recieved messages are "R".
 *
 * Here we receive client id from connections list to detect
 * which messages deal with.
 *
 * Client ID is unique to each connection.
 */
public class MessagesListActivity extends AppCompatActivity {

    // TODO : show dialog to confirm delete operation
    // TODO : delete message with swipe
    // TODO : change from connect to disconnect and vise versa in one button

    // Distictive client id passed from connection list.
    private String clientId;

    // Placeholder for connection client.
    private MqttAndroidClient client;

    // Get and view messages on the screen.
    private MessageItem messageItem;
    private ArrayList<MessageItem> messagesList;
    private MessageAdapter messageAdapter;
    private ListView messageListView;

    // Help in reading , droping and updating database.
    private MessageDbHelperMethods messagesDbHelperMethods;
    private ConnectionDbHelperMethods connectionDbHelperMethods;

    // Store the cursor from messagesDbHelperMethods containing all messages database information.
    private Cursor cursor;

    // Figure out the index of each column while reading cursor from messages database.
    private int clientIdColumnIndex;
    private int messageColumnIndex;
    private int topicColumnIndex;
    private int dateColumnIndex;
    private int timeColumnIndex;
    private int typeColumnIndex;

    // Store data currently read from messages database cursor.
    private String currentClientId;
    private String currentMessage;
    private String currentTopic;
    private String currentDate;
    private String currentTime;
    private String currentType;

    // Adapter to mqttConect to help in passing data and connect it.
    private MqttConnectAdapter connector;

    // Help in disconnecting.
    private MqttDisconnect mqttDisconnect;

    // Get current date and time to insert data on message arrive.
    private Calendar calendar;
    private SimpleDateFormat formater;
    private String date;
    private String time;

    // Notification system.
    private Notification notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        Log.d("messageList activity" , "message list activity has started");

        // Get client id passed from connection list.
        clientId = getIntent().getStringExtra("clienId");

        Log.d("messageList activity" , "client id recieved");

        // Get client from factory map to use it later in checking.
        final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
        this.client = connectionFactory.getSpecificClient(clientId , getApplicationContext());

        Log.d("messageList activity" , "client is fetched from map");

        // Setup FAB to open PublishActivity.
        FloatingActionButton fabPublish = (FloatingActionButton) findViewById(R.id.fab_publish);
        fabPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("messageList activity" , "publish is clicked");

                // Get last client from factory map to use it in checking.
                // It is done again because client status may change.
                client = connectionFactory.getSpecificClient(clientId , getApplicationContext());

                Log.d("messageList activity" , "client is fethced from map");

                // Check if client is connected or not.
                if(client != null){
                    Log.d("messageList activity" , "client is connected");

                    // Setup intent with client id to open publish activity.
                    Intent intent = new Intent(MessagesListActivity.this, PublishActivity.class);
                    intent.putExtra("clienId",clientId);
                    startActivity(intent);
                }else{
                    Toast.makeText(MessagesListActivity.this, "You should connect first!", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Setup FAB to open SubscribeActivity.
        FloatingActionButton fabSubscribe = (FloatingActionButton) findViewById(R.id.fab_subscribe);
        fabSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("messageList activity" , "subscribe button is clicked");

                // Get last client from factory map to use it in checking.
                // It is done again because client status may change.
                client = connectionFactory.getSpecificClient(clientId , getApplicationContext());

                Log.d("messageList activity" , "client is fetched from map");

                // Check if client is connected or not.
                if(client != null){
                    Log.d("messageList activity" , "client is connected");

                    // Setup intent with client id to open subscribe activity.
                    Intent intent = new Intent(MessagesListActivity.this, SubscribeActivity.class);
                    intent.putExtra("clienId",clientId);
                    startActivity(intent);
                }else{
                    Toast.makeText(MessagesListActivity.this, "You should connect first!", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Initialize array list and adapter to use with the list view.
        messagesList = new ArrayList<MessageItem>();
        messageAdapter = new MessageAdapter(this , messagesList);
        messageListView = (ListView) findViewById(R.id.message_list_view);

        Log.d("messageList activity" , "list binded");

        // Setup list view with adapter.
        messageListView.setAdapter(messageAdapter);

        Log.d("messageList activity" , "adapter done");

        // Initialize database helper to help reading and dropping database.
        messagesDbHelperMethods = new MessageDbHelperMethods(this);

        Log.d("messageList activity" , "message helper method is initialized");

        // Long click listener to delete current message.
        messageListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View arg1, int pos, long id) {
                Log.d("messageList activity" , "list view item long clicked");

                // Detect which item was clicked.
                messageItem = messagesList.get(pos);

                Log.d("messageList activity" , "list view item long clicked detected");

                // Delete message from database.
                messagesDbHelperMethods.deleteSpecificMessage(messageItem.getMessage());

                // Update ui.
                messageAdapter.remove(messageItem);

                return true;
            }
        });

        if(client != null){
            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    Log.d("messageList activity" , "client connection lost");

                    // Change connection database status.
                    connectionDbHelperMethods = new ConnectionDbHelperMethods(getApplicationContext());
                    connectionDbHelperMethods.upDateDatabaseStatus(clientId , "D");

                    Log.d("messageList activity" , "database status changed");

                    // Delete client from map.
                    final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
                    connectionFactory.removeSpecificClientFromMap(clientId);

                    Log.d("messageList activity" , "client removed from map");

                    // Change client to disconnected.
                    client = null;

                    Log.d("messageList activity" , "client disconnected");

                    // Notify user.
                    notification = new Notification(clientId , "Connection lost!" , getApplicationContext());

                    Log.d("messageList activity" , "user was notified");
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    Log.d("messageList activity" , "client received a message");

                    // Get current date.
                    calendar = Calendar.getInstance();
                    formater = new SimpleDateFormat("yyyy / MM / dd ");
                    date = formater.format(calendar.getTime());
                    // Get current time.
                    formater = new SimpleDateFormat("HH:mm:ss");
                    time = formater.format(calendar.getTime());

                    // Add message to message database.
                    messagesDbHelperMethods = new MessageDbHelperMethods(getApplicationContext());
                    messagesDbHelperMethods.insertMessage(clientId , topic , message.toString() , date , time , "R");

                    Log.d("messageList activity" , "message inserted to database");

                    // Build message item.
                    messageItem = new MessageItem(message.toString() , topic , date , time ,"R");

                    Log.d("messageList activity" , "message item was build");

                    // UpdateUi
                    messageAdapter.add(messageItem);

                    Log.d("messageList activity" , "message inserted to adapter");

                    // Notify user.
                    notification = new Notification(topic , message.toString() , getApplicationContext());

                    Log.d("messageList activity" , "user was notified");
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    // DO nothing for now
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        updateUi();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // This is done here again to renew content after any change.
        messagesList = new ArrayList<MessageItem>();
        messageAdapter = new MessageAdapter(this , messagesList);
        messageListView = (ListView) findViewById(R.id.message_list_view);

        messageListView.setAdapter(messageAdapter);

        final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
        this.client = connectionFactory.getSpecificClient(clientId , getApplicationContext());

        // Setup FAB to open PublishActivity.
        FloatingActionButton fabPublish = (FloatingActionButton) findViewById(R.id.fab_publish);
        fabPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("messageList activity" , "publish is clicked");

                // Get last client from factory map to use it in checking.
                // It is done again because client status may change.
                client = connectionFactory.getSpecificClient(clientId , getApplicationContext());

                Log.d("messageList activity" , "client is fethced from map");

                // Check if client is connected or not.
                if(client != null){
                    Log.d("messageList activity" , "client is connected");

                    // Setup intent with client id to open publish activity.
                    Intent intent = new Intent(MessagesListActivity.this, PublishActivity.class);
                    intent.putExtra("clienId",clientId);
                    startActivity(intent);
                }else{
                    Toast.makeText(MessagesListActivity.this, "You should connect first!", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Setup FAB to open SubscribeActivity.
        FloatingActionButton fabSubscribe = (FloatingActionButton) findViewById(R.id.fab_subscribe);
        fabSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("messageList activity" , "subscribe button is clicked");

                // Get last client from factory map to use it in checking.
                // It is done again because client status may change.
                client = connectionFactory.getSpecificClient(clientId , getApplicationContext());

                Log.d("messageList activity" , "client is fetched from map");

                // Check if client is connected or not.
                if(client != null){
                    Log.d("messageList activity" , "client is connected");

                    // Setup intent with client id to open subscribe activity.
                    Intent intent = new Intent(MessagesListActivity.this, SubscribeActivity.class);
                    intent.putExtra("clienId",clientId);
                    startActivity(intent);
                }else{
                    Toast.makeText(MessagesListActivity.this, "You should connect first!", Toast.LENGTH_LONG).show();
                }
            }
        });

        if(client != null){
            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    Log.d("messageList activity" , "client connection lost");

                    // Change connection database status.
                    connectionDbHelperMethods = new ConnectionDbHelperMethods(getApplicationContext());
                    connectionDbHelperMethods.upDateDatabaseStatus(clientId , "D");

                    Log.d("messageList activity" , "database status changed");

                    // Delete client from map.
                    final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
                    connectionFactory.removeSpecificClientFromMap(clientId);

                    Log.d("messageList activity" , "client removed from map");

                    // Change client to disconnected.
                    client = null;

                    Log.d("messageList activity" , "client disconnected");

                    // Notify user.
                    notification = new Notification(clientId , "Connection lost!" , getApplicationContext());

                    Log.d("messageList activity" , "user was notified");
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    Log.d("messageList activity" , "client received a message");

                    // Get current date.
                    calendar = Calendar.getInstance();
                    formater = new SimpleDateFormat("yyyy / MM / dd ");
                    date = formater.format(calendar.getTime());
                    // Get current time.
                    formater = new SimpleDateFormat("HH:mm:ss");
                    time = formater.format(calendar.getTime());

                    // Add message to message database.
                    messagesDbHelperMethods = new MessageDbHelperMethods(getApplicationContext());
                    messagesDbHelperMethods.insertMessage(clientId , topic , message.toString() , date , time , "R");

                    Log.d("messageList activity" , "message inserted to database");

                    // Build message item.
                    messageItem = new MessageItem(message.toString() , topic , date , time ,"R");

                    Log.d("messageList activity" , "message item was build");

                    // UpdateUi
                    messageAdapter.add(messageItem);

                    Log.d("messageList activity" , "message inserted to adapter");

                    // Notify user.
                    notification = new Notification(topic , message.toString() , getApplicationContext());

                    Log.d("messageList activity" , "user was notified");
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    // DO nothing for now
                }
            });
        }

        updateUi();
    }

    // Binding option menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.activity_messages_list_menu, menu);
        return true;
    }

    // Setting item on click for option menu.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu.
        switch (item.getItemId()) {
            // Respond to a click on the "Connect" menu option.
            case R.id.message_list_activity_menu_connect_button:
                Log.d("messageList activity" , "connect button was clicked");

                // Connect client.
                connector = new MqttConnectAdapter(clientId ,getApplicationContext());

                Log.d("messageList activity" , "client tried to connect");

                updateUi();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.message_list_activity_menu_delete_all_messages_button:
                Log.d("messageList activity" , "delete all messages was clicked");

                // Delete all client messages
                messagesDbHelperMethods.deleteSpecificClientMessages(clientId);

                Log.d("messageList activity" , "all message deleted");

                // UpdateUi
                messageAdapter.clear();

                return true;
            // Respond to a click on the "Disconnect" button in the app bar
            case R.id.message_list_activity_menu_disconnect_button:
                Log.d("messageList activity" , "disconnect clicked");

                // Get client from factory map to use it later in checking.
                final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
                this.client = connectionFactory.getSpecificClient(clientId , getApplicationContext());

                if(client != null){
                    mqttDisconnect = new MqttDisconnect(client , clientId , getApplicationContext());
                }

                Log.d("messageList activity" , "client is disconnected");
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateUi(){

        Log.d("messageList activity" , "up date ui was called");

        messagesList.clear();

        Log.d("messageList activity" , "message list cleared");

        cursor = messagesDbHelperMethods.readDataBase();

        Log.d("messageList activity" , "cursor is ready with data");

        try{
            // Figure out the index of each column
            clientIdColumnIndex = cursor.getColumnIndex(MessagesContract.MessageEntry.COLUMN_CLIENT_ID);
            messageColumnIndex = cursor.getColumnIndex(MessagesContract.MessageEntry.COLUMN_MESSAGE);
            topicColumnIndex = cursor.getColumnIndex(MessagesContract.MessageEntry.COLUMN_TOPIC);
            dateColumnIndex = cursor.getColumnIndex(MessagesContract.MessageEntry.COLUMN_DATE);
            timeColumnIndex = cursor.getColumnIndex(MessagesContract.MessageEntry.COLUMN_TIME);
            typeColumnIndex = cursor.getColumnIndex(MessagesContract.MessageEntry.COLUMN_TYPE);

            Log.d("messageList activity" , "indexes was figured out");

            while (cursor.moveToNext()) {
                // Use that index to extract the String or Int value of the word
                // at the current row the cursor is on.
                currentClientId = cursor.getString(clientIdColumnIndex);
                if(currentClientId.equals(clientId)){
                    Log.d("messageList activity" , "message client was found");

                    currentMessage = cursor.getString(messageColumnIndex);
                    currentTopic = cursor.getString(topicColumnIndex);
                    currentDate = cursor.getString(dateColumnIndex);
                    currentTime = cursor.getString(timeColumnIndex);
                    currentType = cursor.getString(typeColumnIndex);

                    Log.d("messageList activity" , "message data was fetched");

                    messagesList.add(new MessageItem(currentMessage , currentTopic , currentDate ,
                            currentTime , currentType));

                    Log.d("messageList activity" , "message is done");
                }
            }
        }finally {
            cursor.close();
        }
    }
}
