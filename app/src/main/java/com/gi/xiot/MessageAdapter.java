package com.gi.xiot;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/*
 * Message adapter to pass connection data to screen.
 */

public class MessageAdapter extends ArrayAdapter<MessageItem> {

    public MessageAdapter(Activity context, ArrayList<MessageItem> androidFlavors) {
        super(context, 0, androidFlavors);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.message_item, parent, false);
        }
        MessageItem messageItem = getItem(position);

        TextView messageTextView = (TextView) listItemView.findViewById(R.id.message_text_view);
        messageTextView.setText(messageItem.getMessage());

        TextView topicTextView = (TextView) listItemView.findViewById(R.id.topic_text_view);
        topicTextView.setText(messageItem.getTopic());

        TextView dateTextView = (TextView) listItemView.findViewById(R.id.date_text_view);
        dateTextView.setText(messageItem.getDate());

        TextView timeTextView = (TextView) listItemView.findViewById(R.id.time_text_view);
        timeTextView.setText(messageItem.getTime());

        TextView typeTextView = (TextView) listItemView.findViewById(R.id.message_type_text_view);
        typeTextView.setText(messageItem.getType());

        ImageView typeImageView = (ImageView) listItemView.findViewById(R.id.message_type_circle);
        if(messageItem.getType().equals("R")){
            typeImageView.setImageResource(R.drawable.color_brown);
        }else{
            typeImageView.setImageResource(R.drawable.color_dusty_yellow);
        }

        return listItemView;
    }

}
