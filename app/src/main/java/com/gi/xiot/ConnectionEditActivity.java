package com.gi.xiot;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.gi.xiot.data.ConnectionContract;
import com.gi.xiot.data.ConnectionDbHelper;

import org.eclipse.paho.client.mqttv3.MqttClient;

/*
 * This is responsible to get data from edit connection activity.
 *
 * On done click data is passed to connection database.
 *
 * On advanced click data is sent as an intent to advanced edit connection.
 *
 * Connection is inserted with default "D" disconnected.
 *
 * Data with advanced options are set to "".
 */
public class ConnectionEditActivity extends AppCompatActivity {

    // For binding data from the screen.
    private EditText clientIdEditView;
    private EditText serverEditView;
    private EditText portEditView;
    private CheckBox cleanSeesionCheckBox;
    private Button generateClientIdButton;
    private Button generateserverButton;
    private Button generateportButton;

    // Values edited in the screen.
    private String clientId;
    private String server;
    private String port;
    private int cleanSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_edit);

        Log.d("connectionEdit activity" , "connection edit activity has started");

        clientIdEditView = (EditText) findViewById(R.id.client_id_edit_view);
        serverEditView = (EditText) findViewById(R.id.server_edit_view);
        portEditView = (EditText) findViewById(R.id.port_edit_view);
        cleanSeesionCheckBox = (CheckBox) findViewById(R.id.clean_session_check_box);
        generateClientIdButton = (Button) findViewById(R.id.generate_client_id_button);
        generateserverButton = (Button) findViewById(R.id.generate_server_button);
        generateportButton = (Button) findViewById(R.id.generate_port_button);

        // Generate a random client id using MqttClient which do that.
        generateClientIdButton = (Button) findViewById(R.id.generate_client_id_button);
        generateClientIdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("connectionEdit activity" , "generate random clinet id button clicked");

                String clientId = MqttClient.generateClientId();

                clientIdEditView.setText(clientId);
            }
        });

        // Generate a default server.
        generateserverButton = (Button) findViewById(R.id.generate_server_button);
        generateserverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("connectionEdit activity" , "generate server button clicked");

                serverEditView.setText("broker.hivemq.com");
            }
        });

        // Generate a default port.
        generateportButton= (Button) findViewById(R.id.generate_port_button);
        generateportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("connectionEdit activity" , "generate port button clicked");

                portEditView.setText("1883");
            }
        });

    }

    // Binding option menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.activity_connection_edit_menu, menu);
        return true;
    }

    // Setting item on click for option menu.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu.
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option.
            case R.id.connection_edit_activity_mnue_save_connection_button:
                Log.d("connectionEdit activity" , "save connection button clicked");

                bindDataFromScreen();

                Log.d("connectionEdit activity" , "data binded from screen");

                if(ckeckDataValidity()){
                    Log.d("connectionEdit activity" , "data is checked");

                    // Save connection to database.
                    insertConnectionToDatabase();

                    Log.d("connectionEdit activity" , "data is inserted to database");

                    // Exit activity.
                    finish();
                }else{
                    Toast.makeText(this, "Connection can't be established wrong data!", Toast.LENGTH_LONG).show();
                }
                return true;
            // Respond to a click on the "Delete" menu option.
            case R.id.connection_edit_activity_menu_cancel_button:
                Log.d("connectionEdit activity" , "delete connection button clicked");

                // Exit activity
                finish();
                return true;
            // Respond to a click on the "Advanced" menu option.
            case R.id.connection_edit_activity_mnue_advanced_button:
                Log.d("connectionEdit activity" , "advanced edit connection button clicked");

                bindDataFromScreen();

                Log.d("connectionEdit activity" , "data binded from screen");

                if(ckeckDataValidity()){
                    Log.d("connectionEdit activity" , "data is checked");

                    // Setup intent with data to pass to advanced edit activity.
                    Intent intent = new Intent(ConnectionEditActivity.this, ConnectionAdvancedEditActivity.class);
                    intent.putExtra("clientId",clientId);
                    intent.putExtra("server",server);
                    intent.putExtra("port",port);
                    intent.putExtra("cleanSession",cleanSession);
                    startActivity(intent);
                }else{
                    Toast.makeText(this, "Can't move on complete data!" , Toast.LENGTH_LONG).show();
                }
                return true;
            // Respond to a click on the "Up" arrow button in the app bar.
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity).
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void bindDataFromScreen(){
        clientId = clientIdEditView.getText().toString().trim();
        server = serverEditView.getText().toString().trim();
        port = portEditView.getText().toString().trim();
        if(cleanSeesionCheckBox.isChecked()){
            cleanSession = 1;
        }else{
            cleanSession = 0;
        }
    }

    private boolean ckeckDataValidity(){
        if(clientId.matches("") || server.matches("") || port.matches("")){
            return false;
        }
        return true;
    }

    private void insertConnectionToDatabase(){
        Log.d("connectionEdit activity" ,"start inserting connection");

        // Create database helper
        ConnectionDbHelper mDbHelper = new ConnectionDbHelper(getApplicationContext());

        Log.d("connectionEdit activity" ,"connection helper is initialized");

        // Gets the database in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("connectionEdit activity" ,"database in write mode");

        // Create a ContentValues object where column names are the keys,
        // and pet attributes from the editor are the values.
        ContentValues values = new ContentValues();
        values.put(ConnectionContract.ConnectionEntry.COLUMN_CLIENT_ID, clientId);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_SERVER, server);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_PORT, port);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_CLEAN_SESSION, cleanSession);
        values.put(ConnectionContract.ConnectionEntry.COLUMN_USER_NAME, "");
        values.put(ConnectionContract.ConnectionEntry.COLUMN_PASSWORD, "");
        values.put(ConnectionContract.ConnectionEntry.COLUMN_SSL, "");
        values.put(ConnectionContract.ConnectionEntry.COLUMN_TIME_OUT, "");
        values.put(ConnectionContract.ConnectionEntry.COLUMN_KEEP_ALIVE_TIME, "");
        values.put(ConnectionContract.ConnectionEntry.COLUMN_STATUS, "D");

        Log.d("connectionEdit activity" ,"values are ready with data");

        // Insert a new row for pet in the database, returning the ID of that new row.
        long newRowId = db.insert(ConnectionContract.ConnectionEntry.TABLE_NAME, null, values);

        Log.d("connectionEdit activity" ,"insertion done");

        // Show a toast message depending on whether or not the insertion was successful
        if (newRowId == -1) {
            // If the row ID is -1, then there was an error with insertion.
            Toast.makeText(this, "Error with establishing connection", Toast.LENGTH_SHORT).show();
        } else {
            // Otherwise, the insertion was successful and we can display a toast with the row ID.
            Toast.makeText(this, "Connection is saved", Toast.LENGTH_SHORT).show();
        }
    }
}
