package com.gi.xiot;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;

public class requiredUserPanel extends AppCompatActivity {

    private EditText topic;
    private Button hello;
    private Button subscribe;
    private Button bye;
    private TextView messages;

    private MqttAndroidClient client;

    private IMqttToken  token;

    private String clientId;
    private String url;

    private boolean flagConnection;
    private boolean flagSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_required_user_panel);

        topic = (EditText) findViewById(R.id.user_panel_edit_view);
        hello = (Button) findViewById(R.id.user_panel_hello);
        subscribe = (Button) findViewById(R.id.user_panel_subscribe);
        bye = (Button) findViewById(R.id.user_panel_bye);
        messages = (TextView) findViewById(R.id.user_panel_text_view);

        flagConnection = false;

        connectClient();

        hello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publishMessage("Hello");
            }
        });

        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flagSubscription = false;
                subscribeTopic();
            }
        });

        bye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publishMessage("Bye");
            }
        });

        client.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                Toast.makeText(getApplicationContext(), "Connection is lost", Toast.LENGTH_LONG).show();
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                messages.setText(message.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
    }

    private void connectClient(){

        try {
            clientId = MqttClient.generateClientId();

            url = "tcp://" + "broker.hivemq.com" + ":" + "1883";

            client = new MqttAndroidClient(getApplicationContext(), url, clientId);

            token = client.connect();

            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    if(!flagConnection){
                        flagConnection = true;
                        Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    if(!flagConnection){
                        flagConnection = true;
                        Toast.makeText(getApplicationContext(), "Unable to connect", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void publishMessage(String payload){

        byte[] encodedPayload = new byte[0];

        try {

            encodedPayload = payload.getBytes("UTF-8");

            MqttMessage message = new MqttMessage(encodedPayload);

            client.publish("User Panel", message);

            Toast.makeText(getApplicationContext(), "Published", Toast.LENGTH_LONG).show();

        } catch (UnsupportedEncodingException | MqttException e) {
            e.printStackTrace();
        }
    }

    private void subscribeTopic(){
        String topicText = topic.getText().toString().trim();
        if(!topicText.matches("")){
            try {
                IMqttToken subToken = client.subscribe(topicText, 0);

                subToken.setActionCallback(new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        // The topic was subscribed
                        if(!flagSubscription){
                            flagSubscription = true;
                            Toast.makeText(getApplicationContext(), "Subscribtion is done", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken,
                                          Throwable exception) {
                        // The subscription could not be performed, maybe the user was not
                        // authorized to subscribe on the specified topic e.g. using wildcards
                        if(!flagSubscription){
                            flagSubscription = true;
                            Toast.makeText(getApplicationContext(), "Sorry can't subscribe!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(getApplicationContext(), "Insert topic first", Toast.LENGTH_LONG).show();
        }

    }
}
