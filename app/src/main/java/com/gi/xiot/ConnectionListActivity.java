package com.gi.xiot;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.gi.xiot.data.ConnectionContract;
import com.gi.xiot.data.ConnectionDbHelperMethods;
import com.gi.xiot.data.MessageDbHelperMethods;
import com.gi.xiot.data.SubscribeDbHelperMethods;

import java.util.ArrayList;

/*
 * This is a list activity of all connections.
 *
 * Active connections are "C" connected.
 *
 * Disactive connections are "D" disconnected.
 *
 * Here on clicking on a connection item message list starts with
 * intent sent in it client id to know which connection deal with.
 *
 * Client ID is unique to each connection.
 */
public class ConnectionListActivity extends AppCompatActivity {

    // TODO : show dialog to confirm delete operation
    // TODO : delete connection with swipe

    // Get and view connections on the screen.
    private ConnectionItem connectionItem;
    private ArrayList<ConnectionItem> connectionsList;
    private ConnectionAdapter connectionAdapter;
    private ListView connectionListView;

    // Help in reading and droping database.
    private ConnectionDbHelperMethods connectionDbHelperMethods;
    private MessageDbHelperMethods messageDbHelperMethods;
    private SubscribeDbHelperMethods subscribeDbHelperMethods;

    // Store the cursor from connectionDbHelperMethods containing all connection database information.
    private Cursor cursor;

    // Figure out the index of each column while reading cursor from connection database.
    private int clientIdColumnIndex;
    private int serverColumnIndex;
    private int portColumnIndex;
    private int cleanSessionColumnIndex;
    private int userNameColumnIndex;
    private int passwordColumnIndex;
    private int sslColumnIndex;
    private int timeOutColumnIndex;
    private int keepAliveTimeOutColumnIndex;
    private int statusColumnIndex;

    // Store data currently read from connection database cursor.
    private String currentClientId;
    private String currentServer;
    private String currentPort;
    private int currentCleanSession;
    private String currentUserName;
    private String currentPassword;
    private String currentFilePath;
    private int currentTimeOut;
    private int currentKeepAliveTimeOut;
    private String currentStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_list);

        Log.d("connectionList activity" , "connection list activity has started");

        // Setup FAB to open connectionEditActivity.
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add_connection);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("connectionList activity" , "add connection fab clicked");

                Intent intent = new Intent(ConnectionListActivity.this, ConnectionEditActivity.class);
                startActivity(intent);
            }
        });

        // Initialize array list and adapter to use with the list view.
        connectionsList = new ArrayList<ConnectionItem>();
        connectionAdapter = new ConnectionAdapter(this , connectionsList);
        connectionListView = (ListView) findViewById(R.id.connection_list_view);

        Log.d("connectionList activity" , "list binded");

        // Setup list view with adapter.
        connectionListView.setAdapter(connectionAdapter);

        Log.d("connectionList activity" , "adapter done");

        // Initialize database helper to help reading and dropping database.
        connectionDbHelperMethods = new ConnectionDbHelperMethods(getApplicationContext());
        messageDbHelperMethods = new MessageDbHelperMethods(getApplicationContext());
        subscribeDbHelperMethods = new SubscribeDbHelperMethods(getApplicationContext());

        Log.d("connectionList activity" ,"helper method is initialized");

        // Click listener to open messages list view.
        connectionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("connectionList activity" , "list view item clicked");

                // Detect which item was clicked.
                connectionItem = connectionsList.get(position);

                Log.d("connectionList activity" , "list view clicked item detected");

                // Setup intetn and pass client id with it.
                Intent intent = new Intent(ConnectionListActivity.this, MessagesListActivity.class);
                intent.putExtra("clienId",connectionItem.getClientId());
                startActivity(intent);
            }
        });

        // Long click listener to delete current connection.
        connectionListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View arg1, int pos, long id) {
                Log.d("connectionList activity" , "list view item long clicked");

                // Detect which item was clicked.
                connectionItem = connectionsList.get(pos);

                Log.d("connectionList activity" , "list view long clicked item detected");

                // Delete connection from connection database.
                connectionDbHelperMethods.deleteSpecificConnection(connectionItem.getClientId());

                // Delete messages from database.
                messageDbHelperMethods.deleteSpecificClientMessages(connectionItem.getClientId());

                // Delete subscriptions from database.
                subscribeDbHelperMethods.deleteSpecificClientSubscription(connectionItem.getClientId());

                // Delete client from map if connected.
                final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
                connectionFactory.removeSpecificClientFromMap(connectionItem.getClientId());

                // Update ui.
                connectionAdapter.remove(connectionItem);

                return true;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        updateUi();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // This is done here again to renew adapter content after any change.
        connectionsList = new ArrayList<ConnectionItem>();
        connectionAdapter = new ConnectionAdapter(this , connectionsList);
        connectionListView = (ListView) findViewById(R.id.connection_list_view);

        connectionListView.setAdapter(connectionAdapter);

        connectionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("connectionList activity" , "list view item clicked");

                // Detect which item was clicked.
                connectionItem = connectionsList.get(position);

                Log.d("connectionList activity" , "list view clicked item detected");

                // Setup intetn and pass client id with it.
                Intent intent = new Intent(ConnectionListActivity.this, MessagesListActivity.class);
                intent.putExtra("clienId",connectionItem.getClientId());
                startActivity(intent);
            }
        });

        updateUi();
    }

    // Binding option menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.activity_connection_list_menu, menu);
        return true;
    }

    // Setting item on click for option menu.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu.
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.connection_list_activity_mnue_delete_all_button:
                Log.d("connectionList activity" , "delete all connections button clicked");

                // Drop connection database.
                connectionDbHelperMethods.dropDatabase();

                // Drop messages database.
                messageDbHelperMethods.dropDatabase();

                // Drop subscription database.
                subscribeDbHelperMethods.dropDatabase();

                // Delete all map clients.
                final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
                connectionFactory.removeAllClientsFromMap();

                // To clear the screen from any items.
                connectionAdapter.clear();

                Toast.makeText(this, "All conenctions are deleted", Toast.LENGTH_LONG).show();
                return true;
            // Respond to a click on the "User Panel" button in the app bar
            case R.id.user_panel_button:
                // Navigate back to parent activity (CatalogActivity)
                startActivity(new Intent(ConnectionListActivity.this , requiredUserPanel.class));
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Update screen with the latest data in database.
    private void updateUi(){
        Log.d("connectionList activity" , "update ui is called");

        // Clear all old data and make it eady to new data.
        connectionsList.clear();

        Log.d("connectionList activity" , "connection list are cleared");

        // Gets connections database in read mode
        cursor = connectionDbHelperMethods.readDataBase();

        Log.d("connectionList activity" ,"connection cursor is now ready with data");

        try{
            // Figure out the index of each column
            clientIdColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_CLIENT_ID);
            serverColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_SERVER);
            portColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_PORT);
            cleanSessionColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_CLEAN_SESSION);
            userNameColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_USER_NAME);
            passwordColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_PASSWORD);
            sslColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_SSL);
            timeOutColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_TIME_OUT);
            keepAliveTimeOutColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_KEEP_ALIVE_TIME);
            statusColumnIndex = cursor.getColumnIndex(ConnectionContract.ConnectionEntry.COLUMN_STATUS);

            Log.d("connectionList activity" ,"connection index of each column was figured");

            while (cursor.moveToNext()) {
                // Use that index to extract the String or Int value of the word
                // at the current row the cursor is on.
                currentClientId = cursor.getString(clientIdColumnIndex);
                currentServer = cursor.getString(serverColumnIndex);
                currentPort = cursor.getString(portColumnIndex);;
                currentCleanSession = cursor.getInt(cleanSessionColumnIndex);
                currentUserName = cursor.getString(userNameColumnIndex);
                currentPassword = cursor.getString(passwordColumnIndex);
                currentFilePath = cursor.getString(sslColumnIndex);
                currentTimeOut = cursor.getInt(timeOutColumnIndex);
                currentKeepAliveTimeOut = cursor.getInt(keepAliveTimeOutColumnIndex);
                currentStatus = cursor.getString(statusColumnIndex);

                Log.d("connectionList activity" ,"connection data extracted");

                // Create connection item and add it to the list.
                connectionsList.add(new ConnectionItem(currentClientId , currentServer , currentPort , currentCleanSession ,
                        currentUserName , currentPassword , currentFilePath , currentTimeOut ,
                        currentKeepAliveTimeOut , currentStatus ));

                Log.d("connectionList activity" , currentClientId + " added to list with status " + currentStatus);
            }
        }finally {
            cursor.close();
        }
    }
}
