package com.gi.xiot;

/*
 * This an object which store message data and passed to the adapter list.
 */
public class MessageItem {

    private String message;
    private String topic;
    private String date;
    private String time;
    private String type;

    public MessageItem(String message , String topic , String date , String time , String type ){
        this.message = message;
        this.topic = topic;
        this.date = date;
        this.time = time;
        this.type = type;
    }

    public String getMessage(){
        return message;
    }

    public String getTopic() {
        return topic;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getType() {
        return type;
    }
}
