package com.gi.xiot;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gi.xiot.Mqtt.MqttSubscribe;

import org.eclipse.paho.android.service.MqttAndroidClient;

/*
 * Responsible to get subscribe data from user.
 */
public class SubscribeActivity extends AppCompatActivity {

    // Distinctive client id passed from messages list.
    private String clientId;

    // Spinner used to get qos value from user.
    private Spinner mSpiner;

    // Store spinner value.
    private int qosValuse;

    // For binding data from screen.
    private EditText topicEditView;

    // Used to subscribe.
    private MqttAndroidClient client;

    // Subscribe topics.
    private MqttSubscribe subscriber;

    // Store data from user.
    private String topic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe);

        Log.d("subscribe activity" , "subscribe activity has started");

        // Get client id passed from messages list.
        clientId = getIntent().getStringExtra("clienId");

        Log.d("subscribe activity" , "client id recieved");

        // Get client from factory map to use it later in subscribing.
        final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
        this.client = connectionFactory.getSpecificClient(clientId , getApplicationContext());

        Log.d("subscribe activity" , "client is fetched from map");

        topicEditView = (EditText) findViewById(R.id.subscribe_topic_edit_view);

        mSpiner = (Spinner) findViewById(R.id.subscribe_spinner_qos);

        setupSpinner();
    }

    /**
     * Setup the dropdown spinner that allows the user to select the qos.
     */
    private void setupSpinner() {
        // Create adapter for spinner. The list options are from the String array it will use
        // the spinner will use the default layout.
        ArrayAdapter qosSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.array_subscribe_qos_options, android.R.layout.simple_spinner_item);

        // Specify dropdown layout style - simple list view with 1 item per line.
        qosSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // Apply the adapter to the spinner
        mSpiner.setAdapter(qosSpinnerAdapter);

        // Set the integer mSelected to the constant values.
        mSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.publish_activity_qos_spinner_option_0))) {
                        qosValuse = 0;
                    } else if (selection.equals(getString(R.string.publish_activity_qos_spinner_option_1))) {
                        qosValuse = 1;
                    } else {
                        qosValuse = 2;
                    }
                }
            }

            // Because AdapterView is an abstract class, onNothingSelected must be defined.
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                qosValuse = 0;
            }
        });
    }

    // Binding option menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.activity_subscribe_menu, menu);
        return true;
    }

    // Setting item on click for option menu.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.subscribe_activity_mnue_done:
                Log.d("subscribe activity" , "done button is clicked");

                bindData();

                Log.d("subscribe activity" , "data is binded");

                if(checkValidity()){
                    Log.d("subscribe activity" , "data is valid");

                    // Get client from factory map to use it later in subscribing.
                    final ConnectionFactory connectionFactory = ConnectionFactory.getConnectionFactory();
                    this.client = connectionFactory.getSpecificClient(clientId , getApplicationContext());

                    subscriber = new MqttSubscribe(client , topic , qosValuse , SubscribeActivity.this);

                    finish();
                }else{
                    Toast.makeText(this, "Wrong data can't subscribe!", Toast.LENGTH_LONG).show();
                }
                return true;
            // Respond to a click on the "Cancel" menu option
            case R.id.susbcribe_activity_mnue_cancel:
                // Exit activity
                finish();
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                // Navigate back to parent activity (CatalogActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void bindData(){
        topic = topicEditView.getText().toString().trim();
    }

    private boolean checkValidity(){
        if(!topic.matches("")){
            return true;
        }
        return false;
    }
}
