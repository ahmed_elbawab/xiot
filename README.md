# XIOT Filtration Task
# 1-why I picked the language:
I have chosen to develop the app in android because of the following:
-	I have a very good experience and knowledge in java as I have worked with it for 2 years up till now in my faculty.
-	I have experience with android mobile development as I have been chosen to join the international challenge (Google Udacity challenge Android mobile development) and I have reached to the final stage of first pass with another 1000 student from 100,000 at the beginning of the challenge.
-	I have finished google developer’s android development courses (5 course basics + 1 intermediate and advanced).
-	I have worked with android before in another 2 app one will be finished this week (Teachers’ Assistant).
# 2-Instructions for setting the app up:
To set up the app on android mobile you have 2 choices:
-	Install file called (app-debug.apk) to your phone then it will run immediately and it can be found in the following path (XIOT\app\build\outputs\apk).
-	Download the whole project and run it from android studio.
# 3-Assumptions:
-	The app code contain a lot of “Log.d()” which is used to debug the app and test the logic and I have left them as you can use it to check the app and after finishing they can be commented or deleted out and this will improve the app performance and make it faster. 
-	Comments in the app are used only under need due to (Clean Code) reference.
-	Connected connections are detected with green circle and a letter “C” in it.
-	Disconnected connections are detected with red circle and a letter “D” in it.
-	On connection list on long click on any connection it will be deleted and this is for now as I hope to make it deleted with swipe.
-	Published messages are detected with dusty yellow circle and letter “P” in it.
-	Received messages are detected with brown circle and letter “R” in it.
-	On messages list on long click on any message it will be deleted and this is for now as I hope to make it deleted with swipe.
-	If a phone was subscribing a topic and from the same phone you publish to the same topic 2 messages will be found in the messages list one is published and one is received but on deleting one of them the both will be deleted as it is meaningless to publish and subscribe the same topic from the same device.
-	Required user panel are built with default data saved in the code like topic which is published to (User Panel), server and port and any another data needed to complete the operations.
# 4-Issues with my app:
-	The app is built from zero and I didn’t copy or paste any code from the example code given and I have changed a lot in it from the UI to the logic and also the structure used and I have solved a lot of bugs which were found in it so that toke from me a very long time and I wasn’t able to finish this things:
•	Last will Activity in editing messages.
•	File chooser button in advanced edit activity: I found a lot of libraries online but they were very old and will make the performance of the app very bad and slow so I decided to implement them by myself but I need a long time to be done.
•	Unsubscribe activity: I have built and implemented its logic but I didn’t build an UI activity for it to be shown for user.
# 5-what I wanted to accomplish with my application:
-	First of all I developed the app as I hope to join XIOT team.
-	I hope to use the app in IOT field and begin to communicate with devices.
-	I hope to develop my app to be able to connect with computers and do some operations through them.
-	From the first second I read the task I thought in another usage for the app which is to use the app in small groups communication so I hope to be able to accomplish this.
-	I wanted to accomplish a lot from this app as it is considered my first real app so I was willing to learn new things from it and it was done.
# 6-Thought process as I built the application:
-	I started with reading all the documentations and watching some videos.
-	Then I moved on paper to write what I will need and I divided the app to 3 parts as the following ( Connection list and adding new connection , connecting connection item and moving on to messages list , publish and subscribe and the rest of the app).
-	I started with connection list and build a connection item and database schema for it which was updated more than once while working through the app specially for connection status which was a very hard problem for me to solve, On the example app you gave me when I close the app and open it again all connections are lost which for me I considered as a bug so I tried hardly to solve it and make the app reconnect all connection again.
-	After that I finished messages list which was exhausting somehow in updating UI with new messages.
-	Then I moved on to connect items which was very tricky and causing a lot of bugs in the app you give me, The problem is in the connection call backs which is called more than once by the background service so in the example app there were 2 notifications and 2 created client messages, and after understanding that I used a flag to fix it.
-	At this point from the app I changed my building technique as I found that I need to pass client through classes and store it as the app is live so I created a factory class which is used to build all connection on app start and store them in a map and I used singleton design pattern in it to be only one map through the app.
-	After that I moved to save subscribed topics to be resub scribed after app is closed.  
-	Finally I completed publish, subscribe and the rest of the app which I didn’t face any hard problems in them.
# 7- What I learned in the process:
-	I learned how to analysis others code and notice bugs in them.
-	I learned how to pass information through activities which was first time for me to do it.
-	I learned how to deal with UI in a better way and how to update it.
-	I learned how to use 2 fab buttons above each other.
-	I learned how to use file chooser buttons.
-	I learned how to delete items with swipe.
-	The most thing that I have learned is to finish my work early ^_^.
# 8- How much experience I have with the particular language and framework I chose:
-	I have good experience with java and android studio as I have mentioned above and I always try to improve my knowledge in them and learn new things like flutter, native apps, dealing with APIs.
# 9- Some bugs I found in the example app:
-	Notifications are done twice.
-	Publish messages appear twice in the history.
-	Subscription messages appear twice in the history.
-	Connection items creation appear twice in the history.
-	Publish messages history are deleted after closing the app.
-	Subscription messages history are deleted after closing app.
-	All subscriptions are lost after closing app.
-	Connected items are disconnected after closing app.
-	No unsubscribe activity (Like me ^_^).
-	This error in MQTT broker server as I you tried to connect with a client Id then another device connected with the same client Id the first device disconnected.
